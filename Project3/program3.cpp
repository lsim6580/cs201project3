/*
Luke Simmons
Cs 201
Program 3
Write a class that will keep track of dates and beable to add subtract certain dates. The class will also beable to compare dates.
inputs: input and output file.
1. create a class called Date.
1a.class should either accept no parameters or ints Month,Day,Year.
1b.class should have 3 getters(get Month value, get Day value, get Year value)
1c. Date should have all comparison operators overloaded, two + operators
one for adding integer to date and one to add date to integer, two - operators
one for finding the difference between two dates and to subract integer from date.
the class should also have the << and >> operators overloaded.
2. Date should no how to handle leap years.
3. The main program should read from an input file and handle the operation and output to a file.
*/


#include "Date.h"
#include <fstream>
#include <iostream>
#include <string>
using namespace std;

int main()
{
	string inputfile;
	string outputfile;
	cout << "Please enter the input file: " << endl;
	cin >> inputfile;// input the input file
	cout << "Please enter the output file: " << endl;
	cin >> outputfile;// input the output file
	ifstream fin(inputfile);
	if (!fin.good())// check if the file is good
	{
		cout << " file not found" << endl;
		system("pause");
		return 0;
	}
	Date date;
	ofstream fout(outputfile);
	int i;
	int num;
	Date newnum;
	char chara;
	string str;
	Date B;
	while (fin.good())// loop through and get the values
	{
		fin >> i;
		if (i == 1)// if i = 1
		{
			fin >> date;
			fin >> chara;
			fin >> num;
			if (chara == '+')
			{
				newnum = date + num;
			}
			else{
				newnum = date - num;
			}
			fout << newnum;
		}
		else if (i == 2)// if i = 2
		{
			cout << "i = 2";
			fin >> num;
			fin >> chara;
			fin >> date;
			newnum = num + date;
			fout << newnum;
		}
		else if (i == 3)// if i = 3
		{
			cout << "i = 3";
			fin >> date;
			fin >> chara;
			fin >> B;
			num = date - B;
			fout << num << endl;
		}
		else if (i == 4)// if = 4
		{
			cout << "i = 4";
			fin >> date;
			fin >> str;
			fin >> B;
			if (str == "<")
			{
				newnum = (date < B);
			}
			if (str == ">")
			{
				newnum = (date > B);
			}
			if (str == "!=")
			{
				newnum = (date != B);
			}
			if (str == ">=")
			{
				newnum = (date >= B);
			}
			if (str == "<=")
			{
				newnum = (date <= B);
			}
			while (newnum == 0 || newnum == 1)
			{
				if (newnum == 1)
				{
					fout << "true" << endl;
				}
				if (newnum == 0)
				{
					fout << "false" << endl;
				}
			}
		}
		else cout << "Bad";
	}
	return 0;
}
