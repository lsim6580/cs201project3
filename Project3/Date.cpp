# include "Date.h"
# include <iostream>
using namespace std;

Date::Date()
{
	Yearno = 2000;
	Dayno = 1;
	Monthno = 1;
	NoOfDays = 0;
	convertDate();
}
Date::Date(int NumberofDays)
{
	NoOfDays = NumberofDays;
	convertDate();
}
Date::Date(int Month, int Day, int year)
{
	Yearno = year;
	Dayno = Day;
	Monthno = Month;
	
	NoOfDays =0;
	setNoOfDays(Month,Day,year);
	convertDate();
}
int Date::getDay()
{
	convertDate();
	return Dayno;
}
void Date::setNoOfDays(int Month, int Day, int Year)
{
	int year;
	int month;
	int day;
	int k;
	int i;
	if (Year < 2000)// first we check if the date is before or after 2000(if the year is before)
	{
		NoOfDays -= 365;
		for (k = 1999; k > Year; k--)// subtract the years from the total
		{
			if (checkLeap(k) == true)
			{

				NoOfDays -= 366;
			}
			else
			{
				NoOfDays -= 365;
			}
		}
		for (month = 1; month < Month; month++)//add the months
		{
			if ((checkLeap(k) == true) && (month == 2))
			{
				NoOfDays += 29;
			}
			else {
				NoOfDays += noOfDaysMonth[month];


			}
		}
		NoOfDays = NoOfDays + (Day-2);// add the days
	}
	else// if the date is after 2000
	{
		for (k = 2000; k < Year; k++)// add the years
		{
			if (checkLeap(k) == true)
			{
				NoOfDays += 366;
			}
			else
			{
				NoOfDays += 365;
			}
		}
		for (month = 1; month < Month; month++)// add the months
		{
			if ((checkLeap(k) == true) && (month == 2))
			{
				NoOfDays += 29;
			}
			else {
				NoOfDays += noOfDaysMonth[month];
			}
		}
		NoOfDays = NoOfDays + (Day);// add the date
	}
}
void Date::convertDate()// change number of days into date
{
	int k = 0;
	Yearno = 2000;
	int tempDays = NoOfDays;
	if (NoOfDays >= 0)
	{
		while (tempDays > 0)
		{
			k = 1;
			while (k < 13 && (tempDays >= 0))
			{
				if (checkLeap(Yearno))
				{
					if (k == 2 && (tempDays >= 0))
					{
						tempDays -= 29;
						k++;
					}
					if (!(k == 2) && (tempDays >= 0))
					{
						tempDays -= noOfDaysMonth[k];
						k++;
					}
				}
				if (checkLeap(Yearno) == false && (tempDays >= 0))
				{
					tempDays -= noOfDaysMonth[k];
					k++;
				}

			}
			while (k == 13 && (tempDays > 0))
			{
				tempDays -= noOfDaysMonth[k];
				Yearno++;
				k++;
			}
		}
		Monthno = k - 1;
		Dayno = (tempDays + noOfDaysMonth[Monthno]);
	}
	if (NoOfDays < 0)
	{
		while (tempDays < 0)
		{
			k = 12;
			while (k > 0 && tempDays <= 0)
			{
				if (checkLeap(Yearno) && (tempDays <= 0))
				{
					if (k == 2)
					{
						tempDays += 29;
						k--;
					}
					else
					{
						tempDays += noOfDaysMonth[k];
						k--;
					}
				}
				if (checkLeap(Yearno) == false && tempDays <= 0)
				{
					tempDays += noOfDaysMonth[k];
					k--;
				}
			}
			while (k == 0 && tempDays < 0)
			{
				tempDays += noOfDaysMonth[k];
				Yearno -= 1;
				k = 1;
			}
		}
		Yearno -= 1;
		Monthno = k+1;
		Dayno = tempDays+1;
		if (Dayno == 0)
		{
			Dayno = 1;
		}
	}
	
}

bool Date::checkLeap(int year)// checks for leapYear
{
	return ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0));
}
int Date::getYear() // returns year
{
	convertDate();
	return Yearno;
}
int Date::getMonth()// returns year
{
	convertDate();
	return Monthno;
}
Date Date::operator+(int other)const
{
	Date D = Date();
	D.NoOfDays = NoOfDays + other;
	return D;
}
const Date operator+(const int days, Date& other)
{
	Date D = Date();
	D.NoOfDays = other.NoOfDays + days;
	return D;
}
Date Date::operator-(int other)const
{
	Date D = Date();

	return NoOfDays - other;
}
const int Date::operator-(const Date& other)const
{
	if (NoOfDays - other.NoOfDays >= 0)
	{
		return NoOfDays - other.NoOfDays;
	}
	else 
		return (NoOfDays - other.NoOfDays)*-1;
}
const bool Date::operator < (const Date& other)const
{
	if (NoOfDays < other.NoOfDays)
		return true;
	else
		return false;
}
const bool Date::operator <=(const Date& other)const
{
	if (NoOfDays <= other.NoOfDays)
		return true;
	else
		return false;
}
const bool Date::operator >=(const Date& other)const
{
	if (NoOfDays >= other.NoOfDays)
		return true;
	else 
		return false;
}
const bool Date::operator > (const Date& other)const
{
	if (NoOfDays > other.NoOfDays)
		return true;
	else
		return false;
}
const bool Date::operator == (const Date& other)const
{
	if (NoOfDays == other.NoOfDays)
		return true;
	else
		return false;
}
const bool Date::operator != (const Date& other)const
{
	if (NoOfDays != other.NoOfDays)
		return true;
	else
		return false;
}
Date Date::operator ++(int)
{
	return NoOfDays++;
}
void Date::operator ++ ()
{
	++NoOfDays;
}
Date Date::operator -- (int)
{
	return NoOfDays++;
}
void Date::operator -- ()
{
	--NoOfDays;
}
ostream& operator<<(ostream& out, Date& D)
{
	char sep = '/';
	out << D.getMonth() << sep << D.getDay() << sep << D.getYear()<< endl;
	return out;
}
istream& operator>>(istream& in, Date& D)
{
	int Day, Month, Year;
	char sep;
	in >> Month >> sep >> Day >> sep >> Year;
	D = Date(Month, Day, Year);
	return in;
}