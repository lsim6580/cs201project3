
Time::Time(int H, int M, int S)
{
    unsigned int tmp = 0; 
    
    if (H < 0) 
        H = 0; 
    if (M < 0) 
        M = 0; 
    if (S < 0)
        S = 0; 
TimeRecord = (H*3600) + (M*60) + S; 
} 

void Time::SetTime(int H, int M, int S)
{
    Time(H, M, S); 
}

int Time::GetSecond() const
{
    return TimeRecord%60; 
}


int Time::GetHour() const
{
    return TimeRecord / 3600;
}

int Time::GetMinute() const
{
    return (TimeRecord / 60) % 60;
}


    int Time::operator-(const Time& T) const
{
    int Diff = TimeRecord - T.TimeRecord; 

    return abs( Diff); 
    }

     ostream& operator<<(ostream& out, const Time& T)
     {
         char colon = ':'; 
         out << T.GetHour() << colon << T.GetMinute() << colon
             << T.GetSecond(); 
         return out; 
     }
     istream& operator>>(istream& in, Time& T)
     {
         int H, M, S;
         char sep; 
         in >> H >> sep >> M >> sep >> S; 
         T.SetTime(H, M, S); 
     }


     bool Time::operator < (const Time& T) const
     {
         return (TimeRecord < T.TimeRecord); 
     }