#pragma once

#include <iostream>
using namespace std;

class Date
{
public:
	Date();//Default Constructor
	Date(int numberofDays);// optional Constructor
	Date(int Month, int Day, int Year);// optional Constructor
	void setNoOfDays(int Day,int Month,int Year);// Accepts 3 ints Day Month Year and determines the number of days from 1/1/2000
	bool checkLeap(int year);// takes in a year and checks to see if it is a leap year returns true if it is and false otherwise
	void convertDate();// converts the number of days and puts it into date form
	int getDay(); // returns an int the number of days
	int getYear(); // returns and int the month number
	int getMonth();// returns the int the year
	// Arithmic operators
	Date operator+(int other)const;
	friend const Date operator+(const int days, Date& other);
	Date operator-(int other)const;
	const int operator-(const Date& other)const;
	// comparison operators
	const bool operator < (const Date& other)const;
	const bool operator <=(const Date& other)const;
	const bool operator >=(const Date& other)const;
	const bool operator > (const Date& other)const;
	const bool operator == (const Date& other)const;
	const bool operator != (const Date& other)const;
	//Unary operators
	Date operator ++(int);
	void operator ++ ();
	Date operator -- (int);
	void operator -- ();
	// stream operators
	friend ostream& operator<<(ostream& out,Date& D);
	friend istream& operator>>(istream& in, Date& D);
	int NoOfDays;
private:

	
	int Yearno;
	int Monthno;
	int Dayno;
	
};

static int noOfDaysMonth[14] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31,0 };// contains the number of days in each month
