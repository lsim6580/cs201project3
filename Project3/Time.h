#pragma once 

#include <iostream>
using namespace std; 

class Time
{
public:
    Time(int H = 0, int M = 0, int S = 0);
    void SetTime(int H, int M, int S = 0); 
    int GetHour() const; 
    int GetMinute() const; 
    int GetSecond() const; 
    int operator-(const Time& T) const; 

    bool operator < (const Time& T) const; 
    
private:
    unsigned int TimeRecord; 
    // Syntax: # of seconds since midnight

}; 
 ostream& operator<<(ostream& out, const Time& T); 
 istream& operator>>(istream& in, Time& T); 
